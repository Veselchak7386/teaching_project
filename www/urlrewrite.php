<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/articles/([a-zA-Z_0-9-]+)/(\$|\\?.*)#",
		"RULE" => "ELEMENT_CODE=\$1",
		"PATH" => "/articles/detail.php",
	),
	array(
		"CONDITION" => "#^/articleadd",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/articles/articleadd.php",
	),
	array(
		"CONDITION" => "#^/photo/#",
		"RULE" => "",
		"ID" => "bitrix:photogallery",
		"PATH" => "/photo.php",
		"SORT" => "100",
	),
	array(
		"CONDITION" => "#^/#",
		"RULE" => "",
		"ID" => "bitrix:blog",
		"PATH" => "/index.php",
		"SORT" => "100",
	),
);

?>