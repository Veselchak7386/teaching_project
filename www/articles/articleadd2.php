<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавить статью");
?>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<script src="js/jquery-3.2.1.js"> </script>
	<title>Добавить статью</title>
</head>
<body>
	<form name ="articleadd_frm" method="POST">
		<div class="text">
		Название статьи
		</div>
		<div class="data">
		<input name="Name" type="text" id="Name" value="" required>
		</div><p>
		<div class="text">
		Картинка статьи
		<input name="picture" type="file" id="picture" value="" required>
		</div><p>
		<div class="text">
		Анонс статьи
		</div>
		<div class="bigdata">
		<textarea name="art_short_text" id="art_short_text" required></textarea>
		</div><p>
		<div class="text">
		Текст статьи
		</div>
		<div class="bigdata">
		<textarea name="art_full_text" id="art_full_text" required></textarea>
		</div><p>
		<div class="text">
		Автор статьи
		</div>
		<div class="data">
		<input name="author" type="text" id="author" value="" required>
		</div>
		<div id="err"> </div>
		<p>
		<input name="send" type="button" id="send" value="Создать статью">
	</form>	
	<script src="js/query.js"> </script>
</body>
</html>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>