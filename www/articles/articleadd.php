<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавить статью");
?><?$APPLICATION->IncludeComponent(
	"bitrix:infoportal.element.add.form",
	"",
	Array(
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "Полный текст статьи",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "Тема статьи",
		"CUSTOM_TITLE_NAME" => "Название статьи",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array("1"),
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "articles",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "/articles/",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
		"PROPERTY_CODES" => array("9","NAME","IBLOCK_SECTION","PREVIEW_TEXT","DETAIL_TEXT","DETAIL_PICTURE"),
		"PROPERTY_CODES_REQUIRED" => array("9","NAME","IBLOCK_SECTION","PREVIEW_TEXT","DETAIL_TEXT","DETAIL_PICTURE"),
		"RESIZE_IMAGES" => "Y",
		"SEF_FOLDER" => "/articleadd",
		"SEF_MODE" => "Y",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>